package sg.sklep;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Products extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("products.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String category = req.getParameter("kategoria");
        if (category.equalsIgnoreCase("sprzet wspinaczkowy")) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("climbingGear.jsp");
            dispatcher.forward(req, resp);
        }
        if (category.equalsIgnoreCase("odziez damska") || category.equalsIgnoreCase("odziez meska")) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("clothing.jsp");
            dispatcher.forward(req, resp);
        }
        if (category.equalsIgnoreCase("buty")) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("shoes.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
