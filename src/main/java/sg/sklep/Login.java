package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        ArrayList<String> loginAttempts = new ArrayList<>();
        String key = "loginAttempts";
        if(req.getSession().getAttribute(key) != null) {
            loginAttempts = (ArrayList<String>) req.getSession().getAttribute(key);
        }
        String msg = "Logowanie korzystajac z danych: " + login + "/" + password;
        loginAttempts.add(msg);
        req.getSession().setAttribute(key, loginAttempts);
//        session[key] = loginAttempts;
//        resp.getWriter().println(msg); // refactoring tips: ctrl+w -> ctrl+alt+v (introduce variable)
        PrintWriter out = resp.getWriter();
        for (String entry : loginAttempts) {
            out.println(entry);
        }
    }
}
