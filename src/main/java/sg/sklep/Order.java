package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Order extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("order.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String product = req.getParameter("product");
        int amount = Integer.parseInt(req.getParameter("amount"));
        String address = req.getParameter("address");
        String postCode = req.getParameter("postCode");
        String city = req.getParameter("city");

        int price = 0;
        switch (product) {
            case "product1":
                price = 10;
                break;
            case "product2":
                price = 20;
                break;
            case "product3":
                price = 30;
                break;
        }
        resp.getWriter().println("Przyjeto zam. na " + (amount * price) + "zl");
        resp.getWriter().println("Adres dostawy:\n" + address + "\n" + postCode + "\n" + city);
    }
}
